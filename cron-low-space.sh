#!/bin/bash

## use me like 
## */15 * * * * /bin/bash /etc/custom/cron-low-space-warning-notification/cron-low-space.sh


PATH="/home/$(id -un)/.local/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/snap/bin"
export DBUS_SESSION_BUS_ADDRESS="${DBUS_SESSION_BUS_ADDRESS:-unix:path=/run/user/${UID}/bus}"; 


function lowspace() { 
    PLACE=$1
    MYLIM=$2
    FREESPACE=$(df -m "$PLACE" |sed "s/ \+/ /g"|cut -f4 -d " "|tail -n1 )
    [[ $FREESPACE -lt $MYLIM ]] && ( 
       PRCNTG=$(df -m / |sed "s/ \+/ /g"|cut -f5 -d " "|tail -n1 )
       MSG=$(echo "YOUR SYSTEM IS RUNNING OUT OF DISK SPACE ( usage of $PLACE : $PRCNTG |  MByte FREE:  $FREESPACE  WARN LIMIT: $LOW_LIM (MB) )" )
       notify-send --urgency=critical --app-name=system "LOW SPACE" "$MSG"
    )
}

## warn below 30Gbyte (default) for / (system root)
test -e /etc/.low_space_warn_root   && LOW_LIM=$(cat /etc/.low_space_warn_root )
[[ -z "$LOW_LIM" ]] && LOW_LIM=30000
lowspace "/" $LOW_LIM
LOW_LIM=""

## warn below 30Gbyte (default) for /home
test -e /etc/.low_space_warn_home   && LOW_LIM=$(cat /etc/.low_space_warn_home)
[[ -z "$LOW_LIM" ]] && LOW_LIM=30000
lowspace "/home/" $LOW_LIM
LOW_LIM=""

## warn below 1Gbyte (default) for /tmp
test -e /etc/.low_space_warn_tmp    && LOW_LIM=$(cat /etc/.low_space_warn_tmp)
[[ -z "$LOW_LIM" ]] && LOW_LIM=1000
lowspace "/tmp/" $LOW_LIM
LOW_LIM=""

## warn below 30Mbyte (default) for /etc
test -e /etc/.low_space_warn_etc    && LOW_LIM=$(cat /etc/.low_space_warn_etc)
[[ -z "$LOW_LIM" ]] && LOW_LIM=30
lowspace "/etc/" $LOW_LIM
LOW_LIM=""

## warn below 10Mbyte (default) for /dev/shm
test -e /etc/.low_space_warn_devshm && LOW_LIM=$(cat /etc/.low_space_warn_devshm )
[[ -z "$LOW_LIM" ]] && LOW_LIM=10
lowspace "/dev/shm/" $LOW_LIM


